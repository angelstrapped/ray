from scene import Scene
from vector import Vector3
from color import Color
from camera import Camera
from triangle import Triangle


class OBJ:
	def parse(self, file_handle) -> Scene:
		camera = Camera(Vector3(0.0, 0.0, -9), Vector3(0, 0, 1.0), 3.0)
		
		scene = Scene(camera)
		vertices = []
		normals = []
		
		obj_lines = file_handle.readlines()
		for line in obj_lines:
			words = line.split(" ")
			if words[0] == "v":
				vertices.append(Vector3(float(words[1]), float(words[2]), float(words[3])))
			elif words[0] == "vn":
				normals.append(Vector3(float(words[1]), float(words[2]), float(words[3])))
			elif words[0] == "f":
				# Just casually assume all vertices come before all faces in the .obj file.
				v0 = vertices[int(words[1].split("/")[0]) - 1]
				v1 = vertices[int(words[2].split("/")[0]) - 1]
				v2 = vertices[int(words[3].split("/")[0]) - 1]
				
				triangle = Triangle(v0, v1, v2, Color(0.1, 0.2, 0.2), 1)
				close_vertex_normal = normals[int(words[1].split("/")[2]) - 1]
				if close_vertex_normal.dot(triangle.n) < 0.0:
					triangle.n = 0.0 - triangle.n

				scene.add(triangle)
		return scene