from typing import TextIO

from color import Color

class Image:
	header: str = "P3 %s %s\n%s\n" # Width, height, then color_depth.

	def __init__(self, width: int, height: int, color_depth: int, background: Color = Color(0, 0, 0)):
		self.width = width
		self.height = height
		self.color_depth = color_depth
		self.color_max = color_depth - 1
		self.pixels = [[background for _ in range(width)] for _ in range(height)]
	
	def set_pixel(self, col: int, row: int, color: Color):
		self.pixels[row][col] = color
	
	def get_pixel(self, col: int, row: int) -> Color:
		return self.pixels[row][col]

	def __str__(self):
		output = self.header % self.width, self.height, self.color_depth
		pass

	def write_image(self, image_file: TextIO):
		"""
		print("w: %s\nh: %s" % (self.width, self.height))
		for row in range(len(self.pixels)):
			print(self.pixels[row][0])
		"""

		image_file.write(self.header % (self.width, self.height, self.color_depth))
		for row in self.pixels:
			for pixel in row:
				image_file.write("%s %s %s " % (self._to_depth(pixel.x), self._to_depth(pixel.y), self._to_depth(pixel.z)))
			image_file.write("\n")
	
	def _to_depth(self, value: float) -> int:
		# Scale to color_depth, truncate to integer, clamp to highest and lowest possible value.
		# For fans of rounding: I really don't care about the difference between e.g.
		# red 232 and red 233, or #E80000 and #E90000.
		return max(min(int(value * self.color_max), self.color_max), 0)
