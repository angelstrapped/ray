import unittest
from vector import Vector3

class TestVector3(unittest.TestCase):
	def setUp(self):
		self.v0 = Vector3(0.0, 0.0, 0.0)
		self.v1 = Vector3(1.0, 2.0, 2.0)
		self.v2 = Vector3(1.0, -2.0, -2.0)
		self.v3 = Vector3(-1.0, -2.0, -2.0)
	
	def test_string(self):
		self.assertEqual("a%s" % Vector3(), "a(0.0, 0.0, 0.0)")
	
	def test_default(self):
		self.assertEqual(self.v0.x, Vector3().x)
		self.assertEqual(self.v0.y, Vector3().y)
		self.assertEqual(self.v0.z, Vector3().z)
	
	def test_magnitude(self):
		self.assertEqual(self.v0.magnitude(), 0)
		self.assertEqual(self.v1.magnitude(), 3)
		self.assertEqual(self.v2.magnitude(), 3)
		self.assertEqual(self.v3.magnitude(), 3)
	
	def test_dot(self):
		self.assertEqual(self.v0.dot(self.v0), 0)
		self.assertEqual(self.v1.dot(self.v0), 0)
		self.assertEqual(self.v1.dot(self.v1), 9)
		self.assertEqual(self.v1.dot(self.v2), -7)
	
	def test_addition(self):
		sum_a = self.v1 + self.v2
		self.assertEqual(str(sum_a), "(2.0, 0.0, 0.0)")
		sum_b = self.v0 + self.v0
		self.assertEqual(str(sum_b), "(0.0, 0.0, 0.0)")
		sum_c = self.v1 + 9
		self.assertEqual(str(sum_c), "(10.0, 11.0, 11.0)")
	
	def test_subtraction(self):
		sum_a = self.v1 - self.v2
		self.assertEqual(str(sum_a), "(0.0, 4.0, 4.0)")
		sum_b = self.v0 - self.v0
		self.assertEqual(str(sum_b), "(0.0, 0.0, 0.0)")
		sum_c = self.v1 - 9
		self.assertEqual(str(sum_c), "(-8.0, -7.0, -7.0)")

	def test_multiplication(self):
		sum_a = self.v1 * self.v2
		self.assertEqual(str(sum_a), "(1.0, -4.0, -4.0)")
		sum_b = self.v0 * self.v0
		self.assertEqual(str(sum_b), "(0.0, 0.0, 0.0)")
		sum_c = self.v1 * 9
		self.assertEqual(str(sum_c), "(9.0, 18.0, 18.0)")

	def test_truedivision(self):
		sum_a = self.v1 / 2
		self.assertEqual(str(sum_a), "(0.5, 1.0, 1.0)")
		sum_b = self.v0 / 2
		self.assertEqual(str(sum_b), "(0.0, 0.0, 0.0)")
		sum_c = self.v1 / -2
		self.assertEqual(str(sum_c), "(-0.5, -1.0, -1.0)")
		sum_d = self.v2 / -2
		self.assertEqual(str(sum_d), "(-0.5, 1.0, 1.0)")
	
	def test_intdivision(self):
		sum_a = self.v1 // 2
		self.assertEqual(str(sum_a), "(0.0, 1.0, 1.0)")
		sum_b = self.v0 // 2
		self.assertEqual(str(sum_b), "(0.0, 0.0, 0.0)")
		sum_c = self.v1 // -2
		self.assertEqual(str(sum_c), "(-1.0, -1.0, -1.0)")
		sum_d = self.v2 // -2
		self.assertEqual(str(sum_d), "(-1.0, 1.0, 1.0)")
	
	def test_cross(self):
		self.assertEqual(str(self.v0.cross(self.v0)), "(0.0, 0.0, 0.0)")
		self.assertEqual(str(self.v0.cross(self.v1)), "(0.0, 0.0, 0.0)")
		self.assertEqual(str(self.v1.cross(self.v0)), "(0.0, 0.0, 0.0)")
		self.assertEqual(str(self.v1.cross(self.v1)), "(0.0, 0.0, 0.0)")
		self.assertEqual(str(self.v1.cross(self.v2)), "(0.0, 4.0, -4.0)")
		self.assertEqual(str(self.v2.cross(self.v1)), "(0.0, -4.0, 4.0)")
		self.assertEqual(str(self.v1.cross(self.v3)), "(0.0, 0.0, 0.0)")
		self.assertEqual(str(self.v3.cross(self.v2)), "(0.0, -4.0, 4.0)")

if __name__ == "__main__":
	unittest.main()
