#!/usr/bin/env python3

from vector import Vector3
from image import Image
from color import Color
from camera import Camera
from sphere import Sphere
from scene import Scene
from triangle import Triangle
from raytracer import Raytracer
from obj import OBJ

IMAGE_WIDTH = 256
IMAGE_HEIGHT = 256
COLOR_DEPTH = 256


def main():
	tri = Triangle(Vector3(0.0, 0.0, 0.0), Vector3(1.0, 0.345, 0.0), Vector3(0.345, 1.0, 0.0))
	print(tri.n)
	tri2 = Triangle(Vector3(0.345, 1.0, 0.0), Vector3(1.0, 0.345, 0.0), Vector3(0.0, 0.0, 0.0))
	print(tri2.n)

if __name__ == "__main__":
	main()