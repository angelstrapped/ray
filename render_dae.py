#!/usr/bin/env python3

from vector import Vector3
from image import Image
from color import Color
from camera import Camera
from scene import Scene
from triangle import Triangle
from random_raytracer import Raytracer
from drivers.dae import DAE

IMAGE_WIDTH = 256
IMAGE_HEIGHT = 256
COLOR_DEPTH = 256


def main():
	dae = DAE()

	scene = dae.parse("untitled.dae")

	image = Image(IMAGE_WIDTH, IMAGE_HEIGHT, COLOR_DEPTH)

	renderer = Raytracer(scene)
	renderer.render(image)

	with open("sphere.ppm", 'w') as image_file:
		image.write_image(image_file)

if __name__ == "__main__":
	main()