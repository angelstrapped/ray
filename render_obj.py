#!/usr/bin/env python3

from vector import Vector3
from image import Image
from color import Color
from camera import Camera
from sphere import Sphere
from scene import Scene
from triangle import Triangle
from random_raytracer import Raytracer
from obj import OBJ

IMAGE_WIDTH = 512
IMAGE_HEIGHT = 512
COLOR_DEPTH = 256


def main():
	obj = OBJ()
	
	with open("ico.obj", 'r') as f:
		scene = obj.parse(f)
	
	#sphere = Sphere(Vector3(0.0, 0.0, 50.0), 15.0, Color(0.1, 0.1, 0.1))
	#scene.add(sphere)

	image = Image(IMAGE_WIDTH, IMAGE_HEIGHT, COLOR_DEPTH)

	renderer = Raytracer(scene)
	renderer.render(image)

	with open("sphere.ppm", 'w') as image_file:
		image.write_image(image_file)

if __name__ == "__main__":
	main()