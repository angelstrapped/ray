from math import sqrt
import random

from baseraytracer import BaseRaytracer
from camera import Camera
from scene import Scene
from ray import Ray
from image import Image
from vector import Vector3
from color import Color


class Raytracer(BaseRaytracer):
	def render(self, image: Image):
		FILM_RADIUS = 1.0
		aspect_ratio = image.width / image.height
		
		x_min = -FILM_RADIUS
		x_max = FILM_RADIUS
		
		# Crop height of screen space to aspect ratio.
		y_min = -FILM_RADIUS / aspect_ratio
		y_max = FILM_RADIUS / aspect_ratio
		
		x_step = (x_max - x_min) / (image.width - 1)
		y_step = (y_max - y_min) / (image.height - 1)
		
		black = Color(0, 0, 0)
		
		c = self.scene.active_camera
		
		samples = 3
		total_samples = image.height * image.width * samples
		for i in range(total_samples):
			if i % image.height == 0:
				print("Sample number %s of %s." % (i, total_samples))

			col = random.randrange(image.width)
			row = random.randrange(image.height)
			
			y = y_min + (y_step * row)
			x = x_min + (x_step * col)
			
			ray_origin = c.origin
			pixel_origin = (c.origin + (c.direction * c.focal_length)) - Vector3(x, y, 0.0)
			ray_direction = (pixel_origin - ray_origin).normalized()

			ray = Ray(ray_origin, ray_direction)
			image.set_pixel(col, row, image.get_pixel(col, row) + (self.raytrace(ray, self.scene) / samples))
