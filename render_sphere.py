#!/usr/bin/env python3

from vector import Vector3
from image import Image
from color import Color
from camera import Camera
from sphere import Sphere
from scene import Scene
from triangle import Triangle
from raytracer import Raytracer

IMAGE_WIDTH = 640
IMAGE_HEIGHT = 480
COLOR_DEPTH = 256


def main():
	red = Color(0.7, 0.5, 0.3)
	green = Color(0.4, 0.6, 0.1)
	blue = Color(0.4, 0.1, 0.8)
	sphere = Sphere(Vector3(0.3, -0.4, 8.0), 0.4, green)
	sphere2 = Sphere(Vector3(-0.3, -0.1, 15.0), 0.3, red)
	sphere3 = Sphere(Vector3(-0.2, 0.1, -0.3), 0.1, blue)
	sphere4 = Sphere(Vector3(0.2, 0.1, -0.1), 0.1, blue)
	sphere5 = Sphere(Vector3(-0.2, 0.1, 0.1), 0.1, blue)
	sphere6 = Sphere(Vector3(-0.2, 0.1, 0.3), 0.1, blue)
	sphere7 = Sphere(Vector3(-0.2, 0.1, 0.5), 0.1, blue)
	p0 = Vector3(-1.3, -0.1, 8.0)
	p1 = Vector3(1.3, -0.1, 8.0)
	p2 = Vector3(1.3, 1.1, 8.0)
	p3 = Vector3(-1.3, 1.1, 8.0)
	triangle = Triangle(p0, p1, p2, green / 9)
	triangle2 = Triangle(p3, p2, p0, green / 9, -1)

	camera = Camera(Vector3(0, -0.2, -3), Vector3(0, 0, 1), 4.0)

	scene = Scene(camera)

	scene.add(triangle2)
	scene.add(triangle)
	scene.add(sphere7)
	scene.add(sphere6)
	scene.add(sphere5)
	scene.add(sphere4)
	scene.add(sphere3)
	scene.add(sphere2)
	scene.add(sphere)

	image = Image(IMAGE_WIDTH, IMAGE_HEIGHT, COLOR_DEPTH)

	renderer = Raytracer(scene)
	renderer.render(image)

	with open("sphere.ppm", 'w') as image_file:
		image.write_image(image_file)

if __name__ == "__main__":
	main()