#!/usr/bin/env python3

from vector import Vector3
from image import Image
from color import Color

IMAGE_WIDTH = 3
IMAGE_HEIGHT = 2
COLOR_DEPTH = 256


def main():
	red = Color(1, 0, 0)
	green = Color(0, 1, 0)
	blue = Color(0, 0, 1)

	image = Image(IMAGE_WIDTH, IMAGE_HEIGHT, COLOR_DEPTH)
	image.set_pixel(0, 0, red)
	image.set_pixel(1, 0, green)
	image.set_pixel(2, 0, blue)
	image.set_pixel(0, 1, red + green)
	image.set_pixel(1, 1, red + green + blue)
	image.set_pixel(2, 1, red * 0.001)

	with open("image.ppm", 'w') as image_file:
		image.write_image(image_file)
	
	exit(0)

def one_d_to_two_d(flat_list: list, width: int) -> list:
	two_d_list = []
	for r in range(len(flat_list) // width):
		row = []
		for c in range(width):
			row.append(flat_list[c + width * r])
		two_d_list.append(row)
	return two_d_list

def left_pad(text: str, length: int):
	padded_text = ("0" * length) + text
	return padded_text[-length:]

if __name__ == "__main__":
	main()