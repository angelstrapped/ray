from math import sqrt


class Vector3:
	def __init__(self, x: float = 0.0, y: float = 0.0, z: float = 0.0):
		self.x = x
		self.y = y
		self.z = z
	
	def __str__(self):
		return "(%s, %s, %s)" % (self.x, self.y, self.z)
	
	def __add__(self, other) -> 'Vector3':
		if isinstance(other, Vector3):
			return Vector3(self.x + other.x, self.y + other.y, self.z + other.z)
		elif isinstance(other, int) or isinstance(other, float):
			return Vector3(self.x + other, self.y + other, self.z + other)
	
	def __radd__(self, other) -> 'Vector3':
		return self.__add__(other)
	
	def __sub__(self, other) -> 'Vector3':
		if isinstance(other, Vector3):
			return Vector3(self.x - other.x, self.y - other.y, self.z - other.z)
		elif isinstance(other, int) or isinstance(other, float):
			return Vector3(self.x - other, self.y - other, self.z - other)
	
	def __rsub__(self, other) -> 'Vector3':
		if isinstance(other, Vector3):
			return Vector3(other.x - self.x, other.y - self.y, other.z - self.z)
		elif isinstance(other, int) or isinstance(other, float):
			return Vector3(other - self.x, other - self.y, other - self.z)
	
	def __truediv__(self, other) -> 'Vector3':
		if isinstance(other, Vector3):
			return Vector3(self.x / other.x, self.y / other.y, self.z / other.z)
		elif isinstance(other, int) or isinstance(other, float):
			return Vector3(self.x / other, self.y / other, self.z / other)

	def __floordiv__(self, other) -> 'Vector3':
		if isinstance(other, Vector3):
			return Vector3(self.x // other.x, self.y // other.y, self.z // other.z)
		elif isinstance(other, int) or isinstance(other, float):
			return Vector3(self.x // other, self.y // other, self.z // other)

	def __mul__(self, other) -> 'Vector3':
		if isinstance(other, int) or isinstance(other, float):
			return Vector3(self.x * other, self.y * other, self.z * other)
		if isinstance(other, Vector3):
			return Vector3(self.x * other.x, self.y * other.y, self.z * other.z)
	
	def __rmul__(self, other) -> 'Vector3':
		return self.__mul__(other)

	def magnitude(self) -> float:
		return sqrt(self.dot(self))
	
	def normalized(self) -> 'Vector3':
		return self / self.magnitude()
	
	def dot(self, other: 'Vector3') -> float:
		return self.x * other.x + self.y * other.y + self.z * other.z
	
	def cross(self, other: 'Vector3') -> 'Vector3':
		return Vector3(
			(self.y * other.z) - (self.z * other.y),
			(self.z * other.x) - (self.x * other.z),
			(self.x * other.y) - (self.y * other.x)
		)

			
			
			
			
			
			
			
			
			