from vector import Vector3


class Camera:
	def __init__(self, origin: Vector3 = Vector3(0, 0, 0), direction: Vector3 = Vector3(0, 0, 1), focal_length: float = 10.0):
		self.origin = origin
		self.direction = direction
		self.focal_length = focal_length
