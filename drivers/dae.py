import collada
from math import atan

from triangle import Triangle
from vector import Vector3
from color import Color
from scene import Scene
from camera import Camera

from objects.point_light import PointLight

class DAE:
	def _coordinates_from_matrix(self, matrix) -> Vector3:
		return Vector3(matrix[0][3], matrix[1][3], matrix[2][3])
	
	def _direction_from_matrix(self, matrix, initial: Vector3 = Vector3(0.0, -1.0, 0.0)) -> Vector3:
		# Column major, or whatever.
		return Vector3(
			(matrix[0][0] * initial.x) + (matrix[0][1] * initial.y) + (matrix[0][2] * initial.z),
			(matrix[1][0] * initial.x) + (matrix[1][1] * initial.y) + (matrix[1][2] * initial.z),
			(matrix[2][0] * initial.x) + (matrix[2][1] * initial.y) + (matrix[2][2] * initial.z)
		)

	def _light_from_node(self, node):
		light = node.children[0].light

		coords = self._coordinates_from_matrix(node.matrix)
		direction = self._direction_from_matrix(node.matrix)
		color = Vector3(*light.color) / 1000.0
		brightness = light.constant_att

		return PointLight(coords, direction, color, brightness)

	def _camera_from_node(self, node):
		cam = node.children[0].camera
		camera_coords = self._coordinates_from_matrix(node.matrix)
		camera_direction = self._direction_from_matrix(node.matrix)

		return Camera(camera_coords, camera_direction, atan(cam.xfov))

	def _triangle_from_node(self, node, material):
		# We just need this to get the back-front direction, so just pick the first one.
		normal = Vector3(*node.normals[0])
		vertices = [Vector3(*vertex) for vertex in node.vertices]
		tri = Triangle(*vertices, Color(material.diffuse[0], material.diffuse[1], material.diffuse[2]))
		if normal.dot(tri.n) < 0.0:
			tri.n = -tri.n
		
		return tri

	def parse(self, file_handle) -> Scene:
		data = collada.Collada(file_handle)

		lights = []
		for scene in data.scenes:
			for node in scene.nodes:
				node_type = type(node.children[0])
				if node_type == collada.scene.CameraNode:
					camera = self._camera_from_node(node)
				elif node_type == collada.scene.LightNode:
					lights.append(self._light_from_node(node))

		scene = Scene(camera)
		[scene.add_light(light) for light in lights]

		for geometry in list(data.scene.objects('geometry')):
			material = list(geometry.materialnodebysymbol.values())[0]
			for primitive in list(geometry.primitives()):
				for shape in primitive.shapes():
					if type(shape) == collada.triangleset.Triangle:
						scene.add(self._triangle_from_node(shape, material.target.effect))
		return scene
