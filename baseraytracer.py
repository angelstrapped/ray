from math import sqrt
import random

from camera import Camera
from scene import Scene
from ray import Ray
from image import Image
from vector import Vector3
from color import Color


class BaseRaytracer:
	def __init__(self, scene: Scene):
		self.scene = scene
	
	def in_shadow(self, point: Vector3, light: Vector3, scene: Scene) -> bool:
		ray_direction = (light.origin - point).normalized()
		ray = Ray(point, ray_direction)
		
		hit_distance, hit_mesh = self.find_nearest_mesh(ray, scene)
		if hit_mesh is not None:
			return True

		return False
	
	def raytrace(self, ray, scene):
		# Additive process for when there are multiple passes or whatever.
		color = Color(0, 0, 0)

		hit_distance, hit_mesh = self.find_nearest_mesh(ray, scene)
		if hit_mesh is None:
			# Background color.
			return color + Color(0.1, 0.2, 0.3)
		
		intersection_point = ray.origin + (ray.direction * hit_distance)
		color += self.color_at(hit_mesh, intersection_point, scene)
		
		diffuse_ray = Ray(intersection_point, self.diffuse_bounce_direction(hit_mesh.n))
		
		d_distance, d_mesh = self.find_nearest_mesh(diffuse_ray, scene)
		if d_mesh:
			color += (d_mesh.color * 0.25)
		else:
			color += 0.1
		
		return color
	
	def find_nearest_mesh(self, ray, scene):
		min_distance = None
		hit_mesh = None
		
		# Loop through every mesh and try to intersect with them.
		for mesh in scene.objects:
			distance = mesh.intersect(ray)
			# I assume there's a reason we don't use simply "if distance:", here.
			# Maybe 0.0 is too falsy...?
			if distance is not None:
				# The comparison here appears to be backward, but the render appears more correct now.
				if (hit_mesh is None) or (distance < min_distance):
					min_distance = distance
					hit_mesh = mesh
		return (min_distance, hit_mesh)
	
	def diffuse_bounce_direction(self, normal: Vector3) -> Vector3:
		# Here I'll do somethning vaguely similar to the sphere solution in RTIOW.
		p = 2.0 * Vector3(random.random(), random.random(), random.random()) - Vector3(1.0, 1.0, 1.0)
		return (p + normal).normalized()
	
	def color_at(self, mesh, point, scene):
		color = Color(0.0, 0.0, 0.0)

		# Ambient light.
		color += mesh.color * 0.2

		for light in scene.lights:
			if not self.in_shadow(point, light, scene):
				dot = mesh.normal(point).dot(light.origin - point)
				# Approximately 2% light color.
				blended_color = (mesh.color + (light.color * 0.02)) * 0.98
				color += mesh.color * (light.brightness * (1 / abs(dot)))
		return color