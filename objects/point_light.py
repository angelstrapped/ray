from vector import Vector3
from color import Color

class PointLight:
	def __init__(
		self,
		origin: Vector3 = Vector3(0, 0, 0),
		direction: Vector3 = Vector3(0, 0, 1),
		color: Color = Color(1.0, 1.0, 1.0),
		brightness: float = 0.1
	):
		self.origin = origin
		self.direction = direction
		self.color = color
		self.brightness = brightness