from math import sqrt

from vector import Vector3
from color import Color
from ray import Ray


class Triangle:
	EPSILON = 0.0001

	def __init__(self, v0: Vector3, v1: Vector3, v2: Vector3, color: Color = Color(1, 1, 1), normal_direction = 1):
		self.v0 = v0
		self.v1 = v1
		self.v2 = v2
		self.e1 = v1 - v0
		self.e2 = v2 - v0
		
		self.color = color
		
		self.n = (self.e1.cross(self.e2) * normal_direction).normalized()
	
	def __str__(self):
		return "(%s, %s, %s) %s %s" % (self.v0, self.v1, self.v2, self.n, self.color)
	
	def normal(self, intersection_point: Vector3) -> Vector3:
		return self.n
	
	def intersect(self, ray: Ray):
		""" This is Möller-Trumbore. Didn't bother to understand this algorithm, yet. """
		h = ray.direction.cross(self.e2)
		a = self.e1.dot(h)
		if (a > -self.EPSILON) and (a < self.EPSILON):
			# The ray is basically parallel to the surface of the triangle, so we call it a miss.
			return None
		
		f = 1.0 / a
		s = ray.origin - self.v0
		u = f * s.dot(h)
		if (u < 0.0) or (u > 1.0):
			# ???
			return None
		
		q = s.cross(self.e1)
		v = f * ray.direction.dot(q)
		if (v < 0.0) or (u + v > 1.0):
			return None
		
		t = f * self.e2.dot(q)
		if t > self.EPSILON:
			return t#(ray.origin + (ray.direction * t)).magnitude()

		return None
