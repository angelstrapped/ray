from vector import Vector3


class Ray:
	def __init__(self, origin: Vector3, direction: Vector3, length: float = 2.0):
		self.origin = origin
		self.direction = direction
		self.length = length