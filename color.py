from vector import Vector3


class Color(Vector3):
	pass
"""
	def __init__(self, r: float, g: float, b: float):
		self.x = r
		self.y = g
		self.z = b

	# Something like this for rgb, later.
	@property
	def r(self) -> float:
		return self.x
	
	@r.setter
	def r(self, value: float):
		self.x = value

	@property
	def g(self) -> float:
		return self.y
	
	@g.setter
	def g(self, value: float):
		self.y = value

	@property
	def b(self) -> float:
		return self.z
	
	@b.setter
	def b(self, value: float):
		self.z = value
"""