from math import sqrt

from vector import Vector3
from color import Color
from ray import Ray


class Sphere:
	def __init__(self, origin: Vector3, radius: float, color: Color = Color(1, 1, 1)):
		self.origin = origin
		self.radius = radius
		self.color = color
	
	# Maybe this is really distance_to? I'm confused.
	def distance_from(self, other: Vector3):
		return other.origin - self.origin
	
	def normal(self, intersection_point: Vector3) -> Vector3:
		return self.origin - intersection_point
	
	def intersect(self, ray: Ray):
		distance_from_ray = self.distance_from(ray)
		
		# The variable d represents the positive or negative direction of the ray.
		d = 1

		# Something, something, normal...?
		t = ray.direction.dot(distance_from_ray) * ray.length

		# Maybe something about Pythegoras, since there's a square involved...?
		c = distance_from_ray.dot(distance_from_ray) - (self.radius ** 2)

		# Fgsfds...? 
		discriminant = (t ** 2) - (4 * d * c)

		# Ugh, those three cases...
		if discriminant >= 0:
			# ...
			distance_to_intersection = (-t - sqrt(discriminant)) / (2 * d)
			if distance_to_intersection > 0:
				return distance_to_intersection
		return None
