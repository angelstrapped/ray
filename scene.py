from camera import Camera

class Scene:
	objects = []
	lights = []
	def __init__(self, active_camera: Camera = Camera()):
		self.active_camera = active_camera

	def add(self, obj):
		self.objects.append(obj)
	
	def add_light(self, light):
		self.lights.append(light)